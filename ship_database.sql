--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: cargo_list; Type: TABLE; Schema: public; Owner: Gradergage
--

CREATE TABLE cargo_list (
    cargo_id integer NOT NULL,
    cargo_type_id integer,
    cargo_owner_id integer,
    count integer,
    status integer,
    location integer
);


ALTER TABLE cargo_list OWNER TO "Gradergage";

--
-- Name: cargo_list_cargo_id_seq; Type: SEQUENCE; Schema: public; Owner: Gradergage
--

CREATE SEQUENCE cargo_list_cargo_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cargo_list_cargo_id_seq OWNER TO "Gradergage";

--
-- Name: cargo_list_cargo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: Gradergage
--

ALTER SEQUENCE cargo_list_cargo_id_seq OWNED BY cargo_list.cargo_id;


--
-- Name: cargo_types_list; Type: TABLE; Schema: public; Owner: Gradergage
--

CREATE TABLE cargo_types_list (
    cargo_type_id integer NOT NULL,
    cargo_type character varying(80)
);


ALTER TABLE cargo_types_list OWNER TO "Gradergage";

--
-- Name: colonists; Type: TABLE; Schema: public; Owner: Gradergage
--

CREATE TABLE colonists (
    colonist_id integer NOT NULL,
    name character varying(80),
    surname character varying(80),
    birthdate date,
    sex character varying(20),
    race character varying(30),
    status integer
);


ALTER TABLE colonists OWNER TO "Gradergage";

--
-- Name: colonists_colonist_id_seq; Type: SEQUENCE; Schema: public; Owner: Gradergage
--

CREATE SEQUENCE colonists_colonist_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE colonists_colonist_id_seq OWNER TO "Gradergage";

--
-- Name: colonists_colonist_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: Gradergage
--

ALTER SEQUENCE colonists_colonist_id_seq OWNED BY colonists.colonist_id;


--
-- Name: events; Type: TABLE; Schema: public; Owner: Gradergage
--

CREATE TABLE events (
    event_id integer NOT NULL,
    event_name character varying(80),
    threat integer
);


ALTER TABLE events OWNER TO "Gradergage";

--
-- Name: events_event_id_seq; Type: SEQUENCE; Schema: public; Owner: Gradergage
--

CREATE SEQUENCE events_event_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE events_event_id_seq OWNER TO "Gradergage";

--
-- Name: events_event_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: Gradergage
--

ALTER SEQUENCE events_event_id_seq OWNED BY events.event_id;


--
-- Name: locations; Type: TABLE; Schema: public; Owner: Gradergage
--

CREATE TABLE locations (
    location_id integer NOT NULL,
    location_name character varying(80)
);


ALTER TABLE locations OWNER TO "Gradergage";

--
-- Name: locations_location_id_seq; Type: SEQUENCE; Schema: public; Owner: Gradergage
--

CREATE SEQUENCE locations_location_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE locations_location_id_seq OWNER TO "Gradergage";

--
-- Name: locations_location_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: Gradergage
--

ALTER SEQUENCE locations_location_id_seq OWNED BY locations.location_id;


--
-- Name: messages; Type: TABLE; Schema: public; Owner: Gradergage
--

CREATE TABLE messages (
    message_id integer NOT NULL,
    from_id integer,
    to_id integer,
    time_sent timestamp without time zone,
    time_recieved timestamp without time zone,
    data character varying(80)
);


ALTER TABLE messages OWNER TO "Gradergage";

--
-- Name: messages_message_id_seq; Type: SEQUENCE; Schema: public; Owner: Gradergage
--

CREATE SEQUENCE messages_message_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE messages_message_id_seq OWNER TO "Gradergage";

--
-- Name: messages_message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: Gradergage
--

ALTER SEQUENCE messages_message_id_seq OWNED BY messages.message_id;


--
-- Name: route; Type: TABLE; Schema: public; Owner: Gradergage
--

CREATE TABLE route (
    step_id integer NOT NULL,
    star_system_id integer,
    estimated_date date,
    passing_date date
);


ALTER TABLE route OWNER TO "Gradergage";

--
-- Name: route_step_id_seq; Type: SEQUENCE; Schema: public; Owner: Gradergage
--

CREATE SEQUENCE route_step_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE route_step_id_seq OWNER TO "Gradergage";

--
-- Name: route_step_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: Gradergage
--

ALTER SEQUENCE route_step_id_seq OWNED BY route.step_id;


--
-- Name: ship_team; Type: TABLE; Schema: public; Owner: Gradergage
--

CREATE TABLE ship_team (
    colonist_id integer,
    rank character varying(80),
    experience date
);


ALTER TABLE ship_team OWNER TO "Gradergage";

--
-- Name: star_systems; Type: TABLE; Schema: public; Owner: Gradergage
--

CREATE TABLE star_systems (
    star_system_id integer NOT NULL,
    name integer
);


ALTER TABLE star_systems OWNER TO "Gradergage";

--
-- Name: star_systems_star_system_id_seq; Type: SEQUENCE; Schema: public; Owner: Gradergage
--

CREATE SEQUENCE star_systems_star_system_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE star_systems_star_system_id_seq OWNER TO "Gradergage";

--
-- Name: star_systems_star_system_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: Gradergage
--

ALTER SEQUENCE star_systems_star_system_id_seq OWNED BY star_systems.star_system_id;


--
-- Name: statuses; Type: TABLE; Schema: public; Owner: Gradergage
--

CREATE TABLE statuses (
    id integer NOT NULL,
    status_name character varying(80)
);


ALTER TABLE statuses OWNER TO "Gradergage";

--
-- Name: statuses_id_seq; Type: SEQUENCE; Schema: public; Owner: Gradergage
--

CREATE SEQUENCE statuses_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE statuses_id_seq OWNER TO "Gradergage";

--
-- Name: statuses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: Gradergage
--

ALTER SEQUENCE statuses_id_seq OWNED BY statuses.id;


--
-- Name: system_log; Type: TABLE; Schema: public; Owner: Gradergage
--

CREATE TABLE system_log (
    event_id integer,
    data character varying(100),
    data_id integer,
    "time" timestamp without time zone,
    status integer,
    location integer,
    log_id integer NOT NULL
);


ALTER TABLE system_log OWNER TO "Gradergage";

--
-- Name: system_log_log_id_seq; Type: SEQUENCE; Schema: public; Owner: Gradergage
--

CREATE SEQUENCE system_log_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE system_log_log_id_seq OWNER TO "Gradergage";

--
-- Name: system_log_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: Gradergage
--

ALTER SEQUENCE system_log_log_id_seq OWNED BY system_log.log_id;


--
-- Name: cargo_list cargo_id; Type: DEFAULT; Schema: public; Owner: Gradergage
--

ALTER TABLE ONLY cargo_list ALTER COLUMN cargo_id SET DEFAULT nextval('cargo_list_cargo_id_seq'::regclass);


--
-- Name: colonists colonist_id; Type: DEFAULT; Schema: public; Owner: Gradergage
--

ALTER TABLE ONLY colonists ALTER COLUMN colonist_id SET DEFAULT nextval('colonists_colonist_id_seq'::regclass);


--
-- Name: events event_id; Type: DEFAULT; Schema: public; Owner: Gradergage
--

ALTER TABLE ONLY events ALTER COLUMN event_id SET DEFAULT nextval('events_event_id_seq'::regclass);


--
-- Name: locations location_id; Type: DEFAULT; Schema: public; Owner: Gradergage
--

ALTER TABLE ONLY locations ALTER COLUMN location_id SET DEFAULT nextval('locations_location_id_seq'::regclass);


--
-- Name: messages message_id; Type: DEFAULT; Schema: public; Owner: Gradergage
--

ALTER TABLE ONLY messages ALTER COLUMN message_id SET DEFAULT nextval('messages_message_id_seq'::regclass);


--
-- Name: route step_id; Type: DEFAULT; Schema: public; Owner: Gradergage
--

ALTER TABLE ONLY route ALTER COLUMN step_id SET DEFAULT nextval('route_step_id_seq'::regclass);


--
-- Name: star_systems star_system_id; Type: DEFAULT; Schema: public; Owner: Gradergage
--

ALTER TABLE ONLY star_systems ALTER COLUMN star_system_id SET DEFAULT nextval('star_systems_star_system_id_seq'::regclass);


--
-- Name: statuses id; Type: DEFAULT; Schema: public; Owner: Gradergage
--

ALTER TABLE ONLY statuses ALTER COLUMN id SET DEFAULT nextval('statuses_id_seq'::regclass);


--
-- Name: system_log log_id; Type: DEFAULT; Schema: public; Owner: Gradergage
--

ALTER TABLE ONLY system_log ALTER COLUMN log_id SET DEFAULT nextval('system_log_log_id_seq'::regclass);


--
-- Data for Name: cargo_list; Type: TABLE DATA; Schema: public; Owner: Gradergage
--

COPY cargo_list (cargo_id, cargo_type_id, cargo_owner_id, count, status, location) FROM stdin;
\.


--
-- Data for Name: cargo_types_list; Type: TABLE DATA; Schema: public; Owner: Gradergage
--

COPY cargo_types_list (cargo_type_id, cargo_type) FROM stdin;
\.


--
-- Data for Name: colonists; Type: TABLE DATA; Schema: public; Owner: Gradergage
--

COPY colonists (colonist_id, name, surname, birthdate, sex, race, status) FROM stdin;
\.


--
-- Data for Name: events; Type: TABLE DATA; Schema: public; Owner: Gradergage
--

COPY events (event_id, event_name, threat) FROM stdin;
\.


--
-- Data for Name: locations; Type: TABLE DATA; Schema: public; Owner: Gradergage
--

COPY locations (location_id, location_name) FROM stdin;
\.


--
-- Data for Name: messages; Type: TABLE DATA; Schema: public; Owner: Gradergage
--

COPY messages (message_id, from_id, to_id, time_sent, time_recieved, data) FROM stdin;
\.


--
-- Data for Name: route; Type: TABLE DATA; Schema: public; Owner: Gradergage
--

COPY route (step_id, star_system_id, estimated_date, passing_date) FROM stdin;
\.


--
-- Data for Name: ship_team; Type: TABLE DATA; Schema: public; Owner: Gradergage
--

COPY ship_team (colonist_id, rank, experience) FROM stdin;
\.


--
-- Data for Name: star_systems; Type: TABLE DATA; Schema: public; Owner: Gradergage
--

COPY star_systems (star_system_id, name) FROM stdin;
\.


--
-- Data for Name: statuses; Type: TABLE DATA; Schema: public; Owner: Gradergage
--

COPY statuses (id, status_name) FROM stdin;
\.


--
-- Data for Name: system_log; Type: TABLE DATA; Schema: public; Owner: Gradergage
--

COPY system_log (event_id, data, data_id, "time", status, location, log_id) FROM stdin;
\.


--
-- Name: cargo_list_cargo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: Gradergage
--

SELECT pg_catalog.setval('cargo_list_cargo_id_seq', 1, false);


--
-- Name: colonists_colonist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: Gradergage
--

SELECT pg_catalog.setval('colonists_colonist_id_seq', 1, false);


--
-- Name: events_event_id_seq; Type: SEQUENCE SET; Schema: public; Owner: Gradergage
--

SELECT pg_catalog.setval('events_event_id_seq', 1, false);


--
-- Name: locations_location_id_seq; Type: SEQUENCE SET; Schema: public; Owner: Gradergage
--

SELECT pg_catalog.setval('locations_location_id_seq', 1, false);


--
-- Name: messages_message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: Gradergage
--

SELECT pg_catalog.setval('messages_message_id_seq', 1, false);


--
-- Name: route_step_id_seq; Type: SEQUENCE SET; Schema: public; Owner: Gradergage
--

SELECT pg_catalog.setval('route_step_id_seq', 1, false);


--
-- Name: star_systems_star_system_id_seq; Type: SEQUENCE SET; Schema: public; Owner: Gradergage
--

SELECT pg_catalog.setval('star_systems_star_system_id_seq', 1, false);


--
-- Name: statuses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: Gradergage
--

SELECT pg_catalog.setval('statuses_id_seq', 1, false);


--
-- Name: system_log_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: Gradergage
--

SELECT pg_catalog.setval('system_log_log_id_seq', 1, false);


--
-- Name: cargo_list cargo_list_cargo_id_key; Type: CONSTRAINT; Schema: public; Owner: Gradergage
--

ALTER TABLE ONLY cargo_list
    ADD CONSTRAINT cargo_list_cargo_id_key UNIQUE (cargo_id);


--
-- Name: cargo_types_list cargo_types_list_cargo_type_id_pk; Type: CONSTRAINT; Schema: public; Owner: Gradergage
--

ALTER TABLE ONLY cargo_types_list
    ADD CONSTRAINT cargo_types_list_cargo_type_id_pk PRIMARY KEY (cargo_type_id);


--
-- Name: colonists colonists_colonist_id_key; Type: CONSTRAINT; Schema: public; Owner: Gradergage
--

ALTER TABLE ONLY colonists
    ADD CONSTRAINT colonists_colonist_id_key UNIQUE (colonist_id);


--
-- Name: events events_event_id_key; Type: CONSTRAINT; Schema: public; Owner: Gradergage
--

ALTER TABLE ONLY events
    ADD CONSTRAINT events_event_id_key UNIQUE (event_id);


--
-- Name: locations locations_location_id_key; Type: CONSTRAINT; Schema: public; Owner: Gradergage
--

ALTER TABLE ONLY locations
    ADD CONSTRAINT locations_location_id_key UNIQUE (location_id);


--
-- Name: messages messages_pkey; Type: CONSTRAINT; Schema: public; Owner: Gradergage
--

ALTER TABLE ONLY messages
    ADD CONSTRAINT messages_pkey PRIMARY KEY (message_id);


--
-- Name: route route_step_id_key; Type: CONSTRAINT; Schema: public; Owner: Gradergage
--

ALTER TABLE ONLY route
    ADD CONSTRAINT route_step_id_key UNIQUE (step_id);


--
-- Name: star_systems star_systems_pkey; Type: CONSTRAINT; Schema: public; Owner: Gradergage
--

ALTER TABLE ONLY star_systems
    ADD CONSTRAINT star_systems_pkey PRIMARY KEY (star_system_id);


--
-- Name: statuses statuses_id_key; Type: CONSTRAINT; Schema: public; Owner: Gradergage
--

ALTER TABLE ONLY statuses
    ADD CONSTRAINT statuses_id_key UNIQUE (id);


--
-- Name: system_log system_log_log_id_key; Type: CONSTRAINT; Schema: public; Owner: Gradergage
--

ALTER TABLE ONLY system_log
    ADD CONSTRAINT system_log_log_id_key UNIQUE (log_id);


--
-- Name: cargo_types_list_cargo_type_id_uindex; Type: INDEX; Schema: public; Owner: Gradergage
--

CREATE UNIQUE INDEX cargo_types_list_cargo_type_id_uindex ON cargo_types_list USING btree (cargo_type_id);


--
-- Name: messages_message_id_uindex; Type: INDEX; Schema: public; Owner: Gradergage
--

CREATE UNIQUE INDEX messages_message_id_uindex ON messages USING btree (message_id);


--
-- Name: star_systems_star_system_id_uindex; Type: INDEX; Schema: public; Owner: Gradergage
--

CREATE UNIQUE INDEX star_systems_star_system_id_uindex ON star_systems USING btree (star_system_id);


--
-- Name: cargo_list cargo_location; Type: FK CONSTRAINT; Schema: public; Owner: Gradergage
--

ALTER TABLE ONLY cargo_list
    ADD CONSTRAINT cargo_location FOREIGN KEY (location) REFERENCES locations(location_id);


--
-- Name: cargo_list cargo_owner_id; Type: FK CONSTRAINT; Schema: public; Owner: Gradergage
--

ALTER TABLE ONLY cargo_list
    ADD CONSTRAINT cargo_owner_id FOREIGN KEY (cargo_owner_id) REFERENCES colonists(colonist_id);


--
-- Name: cargo_list cargo_status; Type: FK CONSTRAINT; Schema: public; Owner: Gradergage
--

ALTER TABLE ONLY cargo_list
    ADD CONSTRAINT cargo_status FOREIGN KEY (status) REFERENCES statuses(id);


--
-- Name: cargo_list cargo_type_id; Type: FK CONSTRAINT; Schema: public; Owner: Gradergage
--

ALTER TABLE ONLY cargo_list
    ADD CONSTRAINT cargo_type_id FOREIGN KEY (cargo_type_id) REFERENCES cargo_types_list(cargo_type_id);


--
-- Name: ship_team colonists_team; Type: FK CONSTRAINT; Schema: public; Owner: Gradergage
--

ALTER TABLE ONLY ship_team
    ADD CONSTRAINT colonists_team FOREIGN KEY (colonist_id) REFERENCES colonists(colonist_id);


--
-- Name: system_log event_id; Type: FK CONSTRAINT; Schema: public; Owner: Gradergage
--

ALTER TABLE ONLY system_log
    ADD CONSTRAINT event_id FOREIGN KEY (event_id) REFERENCES events(event_id);


--
-- Name: system_log location; Type: FK CONSTRAINT; Schema: public; Owner: Gradergage
--

ALTER TABLE ONLY system_log
    ADD CONSTRAINT location FOREIGN KEY (location) REFERENCES locations(location_id);


--
-- Name: route route; Type: FK CONSTRAINT; Schema: public; Owner: Gradergage
--

ALTER TABLE ONLY route
    ADD CONSTRAINT route FOREIGN KEY (star_system_id) REFERENCES star_systems(star_system_id);


--
-- Name: system_log status_log; Type: FK CONSTRAINT; Schema: public; Owner: Gradergage
--

ALTER TABLE ONLY system_log
    ADD CONSTRAINT status_log FOREIGN KEY (status) REFERENCES statuses(id);


--
-- PostgreSQL database dump complete
--

